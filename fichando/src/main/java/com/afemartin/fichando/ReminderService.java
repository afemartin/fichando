package com.afemartin.fichando;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

public class ReminderService extends IntentService
{

    public ReminderService()
    {
        super("ReminderService");
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        // build notification
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(getString(R.string.notification_title) + ' ' + intent.getStringExtra("estimatedDayEnd"))
                .setContentText(getString(R.string.notification_message))
                .setAutoCancel(true);

        // gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // builds the notification and issues it.
        mNotifyMgr.notify(0, mBuilder.build());
    }

}
