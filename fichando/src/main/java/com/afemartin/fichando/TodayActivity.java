package com.afemartin.fichando;

import java.util.Calendar;
import java.util.StringTokenizer;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.Time;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

public class TodayActivity extends Activity
{
    // Constants
    private static final String TIME_FORMAT = "%H:%M";
    private static final String DATE_FORMAT = "%Y/%m/%d";

    private static final int TIME_DAY_START = 1;
    private static final int TIME_LUNCH_START = 2;
    private static final int TIME_LUNCH_END = 3;
    private static final int TIME_DAY_END = 4;

    private TextView timeDayStart;
    private TextView timeLunchStart;
    private TextView timeLunchEnd;
    private TextView timeDayEnd;
    private TextView timeEstimatedDayEnd;

    private Button buttonDayStart;
    private Button buttonLunchStart;
    private Button buttonLunchEnd;
    private Button buttonDayEnd;

    private TimePickerDialog timePickerDialog;

    private PunchcardModel today;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_today);

        DatabaseHandler db = new DatabaseHandler(this);

        timeDayStart = (TextView) findViewById(R.id.timeDayStart);
        timeLunchStart = (TextView) findViewById(R.id.timeLunchStart);
        timeLunchEnd = (TextView) findViewById(R.id.timeLunchEnd);
        timeDayEnd = (TextView) findViewById(R.id.timeDayEnd);
        timeEstimatedDayEnd = (TextView) findViewById(R.id.timeEstimatedDayEnd);

        buttonDayStart = (Button) findViewById(R.id.buttonDayStart);
        buttonLunchStart = (Button) findViewById(R.id.buttonLunchStart);
        buttonLunchEnd = (Button) findViewById(R.id.buttonLunchEnd);
        buttonDayEnd = (Button) findViewById(R.id.buttonDayEnd);

        // get today date
        Time now = new Time();
        now.setToNow();

        // only display the necessary buttons based on the day of the week
        if (now.weekDay == Time.FRIDAY)
        {
            buttonLunchStart.setEnabled(false);
            buttonLunchEnd.setEnabled(false);
        }
        else if (now.weekDay == Time.SATURDAY || now.weekDay == Time.SUNDAY)
        {
            buttonDayStart.setEnabled(false);
            buttonLunchStart.setEnabled(false);
            buttonLunchEnd.setEnabled(false);
            buttonDayEnd.setEnabled(false);

            // exit withoutread or write from the database
            return;
        }

        // load from database today's information, if exist
        // write it and set as disabled check in/out buttons

        today = db.getPunchcard(now.format(DATE_FORMAT));

        if (today == null)
        {
            today = new PunchcardModel(now.format(DATE_FORMAT));
            db.addPunchcard(today);
        }
        else
        {
            // initialize the information displayed
            if (today.getDayStart() != null && today.getDayStart() != "")
            {
                buttonDayStart.setEnabled(false);
                timeDayStart.setText(today.getDayStart());
                timeDayStart.setClickable(true);
            }
            if (today.getLunchStart() != null && today.getLunchStart() != "")
            {
                buttonLunchStart.setEnabled(false);
                timeLunchStart.setText(today.getLunchStart());
                timeLunchStart.setClickable(true);
            }
            if (today.getLunchEnd() != null && today.getLunchEnd() != "")
            {
                buttonLunchEnd.setEnabled(false);
                timeLunchEnd.setText(today.getLunchEnd());
                timeLunchEnd.setClickable(true);
            }
            if (today.getDayEnd() != null && today.getDayEnd() != "")
            {
                buttonDayEnd.setEnabled(false);
                timeDayEnd.setText(today.getDayEnd());
                timeDayEnd.setClickable(true);
            }

            updateEstimatedDayEndTime();
        }
    }

    public void dayCheckIn(View v)
    {
        // update day start time into textview & database
        updateTime(TIME_DAY_START, timeDayStart);

        // disable check in button and enable edit time dialog
        buttonDayStart.setEnabled(false);
        timeDayStart.setClickable(true);
    }

    public void dayCheckInEdit(View v)
    {
        showTimePickerDialog(v, TIME_DAY_START, timeDayStart);
    }

    public void lunchCheckOut(View v)
    {
        // update lunch start time into textview & database
        updateTime(TIME_LUNCH_START, timeLunchStart);

        // disable check out button and enable edit time dialog
        buttonLunchStart.setEnabled(false);
        timeLunchStart.setClickable(true);
    }

    public void lunchCheckOutEdit(View v)
    {
        showTimePickerDialog(v, TIME_LUNCH_START, timeLunchStart);
    }

    public void lunchCheckIn(View v)
    {
        // update lunch end time into textview & database
        updateTime(TIME_LUNCH_END, timeLunchEnd);

        // disable check in button and enable edit time dialog
        buttonLunchEnd.setEnabled(false);
        timeLunchEnd.setClickable(true);
    }

    public void lunchCheckInEdit(View v)
    {
        showTimePickerDialog(v, TIME_LUNCH_END, timeLunchEnd);
    }

    public void dayCheckOut(View v)
    {
        // update day end time into textview & database
        updateTime(TIME_DAY_END, timeDayEnd);

        // disable check out button and enable edit time dialog
        buttonDayEnd.setEnabled(false);
        timeDayEnd.setClickable(true);
    }

    public void dayCheckOutEdit(View v)
    {
        showTimePickerDialog(v, TIME_DAY_END, timeDayEnd);
    }

    private void showTimePickerDialog(View v, int type, TextView label)
    {
        String time = label.getText().toString();

        StringTokenizer st = new StringTokenizer(time, ":");
        String hour = st.nextToken();
        String minute = st.nextToken();

        timePickerDialog = new TimePickerDialog(v.getContext(),
                new TimePickerHandler(type, label), Integer.parseInt(hour),
                Integer.parseInt(minute), true);

        timePickerDialog.show();
    }

    private void updateTime(int type, TextView label)
    {
        Time now = new Time();
        now.setToNow();
        updateTime(type, label, now.format(TIME_FORMAT));
    }

    private void updateTime(int type, TextView label, String time)
    {
        DatabaseHandler db = new DatabaseHandler(this);

        // set current time into textview
        label.setText(time);

        // set current time into database
        switch (type)
        {
            case TIME_DAY_START:
                today.setDayStart(time);
                break;
            case TIME_LUNCH_START:
                today.setLunchStart(time);
                break;
            case TIME_LUNCH_END:
                today.setLunchEnd(time);
                break;
            case TIME_DAY_END:
                today.setDayEnd(time);
                break;
        }

        // updating today punchcard info
        db.updatePunchcard(today);

        // update the estimated exit time
        updateEstimatedDayEndTime();
    }

    private void updateEstimatedDayEndTime()
    {
        String timeDayStart = today.getDayStart();
        String timeLunchStart = today.getLunchStart();
        String timeLunchEnd = today.getLunchEnd();

        // TODO move to settings
        String workingHours = "8";
        String workingMinutes = "30";

        Time now = new Time();
        now.setToNow();

        if (now.weekDay == Time.FRIDAY)
        {
            // TODO move to settings
            workingHours = "6";
            workingMinutes = "00";
        }

        if (today.getDayStart() != null)
        {
            StringTokenizer st = new StringTokenizer(timeDayStart, ":");
            int minutesDayStart = timeToMins(st.nextToken(), st.nextToken());

            int minutesWorkDuration = timeToMins(workingHours, workingMinutes);

            if (today.getLunchStart() != null && today.getLunchEnd() != null)
            {
                st = new StringTokenizer(timeLunchStart, ":");
                int minutesLunchStart = timeToMins(st.nextToken(), st.nextToken());
                st = new StringTokenizer(timeLunchEnd, ":");
                int minutesLunchEnd = timeToMins(st.nextToken(), st.nextToken());

                // TODO move to settings
                int minimumLunchDuration = 60;

                int minutesLunchDuration = Math.max(minutesLunchEnd - minutesLunchStart, minimumLunchDuration);

                minutesWorkDuration = minutesWorkDuration + minutesLunchDuration;
            }

            int minutesEstimatedDayEnd = minutesDayStart + minutesWorkDuration;

            timeEstimatedDayEnd.setText((CharSequence) minsToTime(minutesEstimatedDayEnd));

            // set notification 5 minutes before estimated day end

            Intent intent = new Intent(this, ReminderService.class);
            intent.putExtra("estimatedDayEnd", minsToTime(minutesEstimatedDayEnd));

            PendingIntent pIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

            st = new StringTokenizer(minsToTime(minutesEstimatedDayEnd), ":");

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(st.nextToken()));
            calendar.set(Calendar.MINUTE, Integer.parseInt(st.nextToken()));
            calendar.add(Calendar.MINUTE, -5);

            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pIntent);
        }
    }

    private int timeToMins(String hour, String minute)
    {
        return Integer.parseInt(hour) * 60 + Integer.parseInt(minute);
    }

    private String minsToTime(int minutes)
    {
        return pad((int) minutes / 60) + ":" + pad((int) minutes % 60);
    }

    private static String pad(int c)
    {
        if (c >= 10)
        {
            return String.valueOf(c);
        }
        else
        {
            return "0" + String.valueOf(c);
        }
    }

    private class TimePickerHandler implements OnTimeSetListener
    {
        private int type;
        private TextView label;

        // flag to avoid run twice thr onTimeSet event due to a buggy behaviour
        private boolean done;

        public TimePickerHandler(int type, TextView label)
        {
            this.type = type;
            this.label = label;
            this.done = false;
        }

        public void onTimeSet(TimePicker view, int hour, int minute)
        {
            if (done) return;

            updateTime(type, label, pad(hour) + ":" + pad(minute));
            done = true;
        }
    }

}
