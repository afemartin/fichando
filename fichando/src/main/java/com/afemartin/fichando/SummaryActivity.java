package com.afemartin.fichando;

import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SummaryActivity extends Activity
{

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        DatabaseHandler db = new DatabaseHandler(this);

        List<PunchcardModel> listOfDays = db.getAllPunchcards();

        LinearLayout layout = (LinearLayout) findViewById(R.id.summaryLayout);

        layout.setPadding(5, 10, 5, 10);

        for (PunchcardModel punchcard : listOfDays)
        {
            if (punchcard.getDayStart() != null)
            {
                final TextView textView = new TextView(this);

                int mins = punchcard.getWorkedExtraMins();

                String extra = "0";

                if (mins > 0)
                {
                    extra = "+" + String.valueOf(mins);
                    textView.setTextColor(0xff77cc55);
                }
                else if (mins < 0)
                {
                    extra = String.valueOf(mins);
                    textView.setTextColor(0xffff6666);
                }

                textView.setText(punchcard.getDate() + ": "
                        + punchcard.getDayStart() + ","
                        + punchcard.getLunchStart() + ","
                        + punchcard.getLunchEnd() + ","
                        + punchcard.getDayEnd() + " (" + extra + ")");
                textView.setPadding(5, 5, 5, 5);
                layout.addView(textView);
            }
        }
    }
}
