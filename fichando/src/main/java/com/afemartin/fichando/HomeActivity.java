package com.afemartin.fichando;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class HomeActivity extends Activity
{

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }

    /**
     * Called when the user clicks the Today button
     *
     * @param view
     */
    public void goTodayActivity(View view)
    {
        startActivity(new Intent(this, TodayActivity.class));
    }

    /**
     * Called when the user clicks the Summary button
     *
     * @param view
     */
    public void goSummaryActivity(View view)
    {
        startActivity(new Intent(this, SummaryActivity.class));
    }

    /**
     * Called when the user clicks the Settings button
     *
     * @param view
     */
    public void goSettingsActivity(View view)
    {
        startActivity(new Intent(this, SettingsActivity.class));
    }
}
