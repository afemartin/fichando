package com.afemartin.fichando;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper
{

    /**
     * **********************************************************************
     * All Static variables
     * ***********************************************************************
     */

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "fichando";

    // Punchcards table name
    private static final String TABLE_PUNCHCARD = "punchcards";

    // Contacts Table Columns names
    private static final String KEY_DATE = "date";
    private static final String KEY_DAY_START = "day_start";
    private static final String KEY_LUNCH_START = "lunch_start";
    private static final String KEY_LUNCH_END = "lunch_end";
    private static final String KEY_DAY_END = "day_end";

    /**
     * Constructor
     *
     * @param context
     */
    public DatabaseHandler(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_PUNCHCARD + "("
                + KEY_DATE + " TEXT PRIMARY KEY," + KEY_DAY_START + " TEXT,"
                + KEY_LUNCH_START + " TEXT," + KEY_LUNCH_END + " TEXT,"
                + KEY_DAY_END + " TEXT" + ")";

        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PUNCHCARD);

        // Create tables again
        onCreate(db);
    }

    /*************************************************************************
     * All CRUD (Create, Read, Update, Delete) Operations
     *************************************************************************/

    /**
     * Adding new punchcard
     *
     * @param punchcard
     */
    void addPunchcard(PunchcardModel punchcard)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_DATE, punchcard.getDate());

        // Inserting Row
        db.insert(TABLE_PUNCHCARD, null, values);
        db.close(); // Closing database connection
    }

    /**
     * Getting single punchcard
     *
     * @param date YYYY/MM/DD
     * @return
     */
    PunchcardModel getPunchcard(String date)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_PUNCHCARD, new String[]{KEY_DATE,
                KEY_DAY_START, KEY_LUNCH_START, KEY_LUNCH_END, KEY_DAY_END},
                KEY_DATE + " = ?", new String[]{date}, null, null, null,
                null);

        if (cursor != null && cursor.getCount() > 0)
        {
            cursor.moveToFirst();

            PunchcardModel punchcard = new PunchcardModel(cursor.getString(0),
                    cursor.getString(1), cursor.getString(2),
                    cursor.getString(3), cursor.getString(4));

            // return punchcard
            return punchcard;
        }
        else
        {
            return null;
        }

    }

    // Getting All Punchcards
    public List<PunchcardModel> getAllPunchcards()
    {
        List<PunchcardModel> punchcardList = new ArrayList<PunchcardModel>();

        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_PUNCHCARD;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // Looping through all rows and adding to list
        if (cursor.moveToFirst())
        {
            do
            {
                PunchcardModel punchcard = new PunchcardModel(cursor.getString(0),
                        cursor.getString(1), cursor.getString(2),
                        cursor.getString(3), cursor.getString(4));
                // Adding punchcard to list
                punchcardList.add(punchcard);
            } while (cursor.moveToNext());
        }

        // return punchcard list
        return punchcardList;
    }

    // Updating single punchcard
    public int updatePunchcard(PunchcardModel punchcard)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_DAY_START, punchcard.getDayStart());
        values.put(KEY_LUNCH_START, punchcard.getLunchStart());
        values.put(KEY_LUNCH_END, punchcard.getLunchEnd());
        values.put(KEY_DAY_END, punchcard.getDayEnd());

        // Updating row
        return db.update(TABLE_PUNCHCARD, values, KEY_DATE + " = ?", new String[]{punchcard.getDate()});
    }

    // Deleting single punchcard
    public void deletePunchcard(PunchcardModel punchcard)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_PUNCHCARD, KEY_DATE + " = ?", new String[]{punchcard.getDate()});
        db.close();
    }

    // Getting punchcards Count
    public int getPunchcardsCount()
    {
        String countQuery = "SELECT * FROM " + TABLE_PUNCHCARD;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        return cursor.getCount();
    }

}
