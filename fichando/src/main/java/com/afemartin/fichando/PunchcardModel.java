package com.afemartin.fichando;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

public class PunchcardModel
{

    // Private variables
    String _date;
    String _dayStart;
    String _lunchStart;
    String _lunchEnd;
    String _dayEnd;

    /**
     * Constructor with key value
     *
     * @param date YYYY/MM/DD
     */
    public PunchcardModel(String date)
    {
        this._date = date;
    }

    /**
     * Constructor with all values
     *
     * @param date       YYYY/MM/DD
     * @param dayStart   HH:MM
     * @param lunchStart HH:MM
     * @param lunchEnd   HH:MM
     * @param dayEnd     HH:MM
     */
    public PunchcardModel(String date, String dayStart, String lunchStart, String lunchEnd, String dayEnd)
    {
        this._date = date;
        this._dayStart = dayStart;
        this._lunchStart = lunchStart;
        this._lunchEnd = lunchEnd;
        this._dayEnd = dayEnd;
    }

    /**
     * Retrieve the date of the punchcard
     *
     * @return string YYYY/MM/DD
     */
    public String getDate()
    {
        return this._date;
    }

    /**
     * Retrieve the dayStart of the punchcard
     *
     * @return string HH:MM
     */
    public String getDayStart()
    {
        return this._dayStart;
    }

    /**
     * Store the dayStart of the punchcard
     *
     * @param dayStart HH:MM
     */
    public void setDayStart(String dayStart)
    {
        this._dayStart = dayStart;
    }

    /**
     * Retrieve the lunchStart of the punchcard
     *
     * @return string HH:MM
     */
    public String getLunchStart()
    {
        return this._lunchStart;
    }

    /**
     * Store the lunchStart of the punchcard
     *
     * @param lunchStart HH:MM
     */
    public void setLunchStart(String lunchStart)
    {
        this._lunchStart = lunchStart;
    }

    /**
     * Retrieve the lunchEnd of the punchcard
     *
     * @return string HH:MM
     */
    public String getLunchEnd()
    {
        return this._lunchEnd;
    }

    /**
     * Store the lunchEnd of the punchcard
     *
     * @param lunchEnd HH:MM
     */
    public void setLunchEnd(String lunchEnd)
    {
        this._lunchEnd = lunchEnd;
    }

    /**
     * Retrieve the dayEnd of the punchcard
     *
     * @return string HH:MM
     */
    public String getDayEnd()
    {
        return this._dayEnd;
    }

    /**
     * Store the dayEnd of the punchcard
     *
     * @param dayEnd HH:MM
     */
    public void setDayEnd(String dayEnd)
    {
        this._dayEnd = dayEnd;
    }

    /**
     * Retrieve the worked extra time of the punchcard
     *
     * @return int minutes
     */
    public int getWorkedExtraMins()
    {
        String timeDayStart = this._dayStart;
        String timeLunchStart = this._lunchStart;
        String timeLunchEnd = this._lunchEnd;
        String timeDayEnd = this._dayEnd;

        // TODO move to settings
        String workingHours = "8";
        String workingMinutes = "30";

        Date date = null;

        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        try
        {
            date = format.parse(this._date);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY)
        {
            // TODO move to settings
            workingHours = "6";
            workingMinutes = "00";
        }

        if (this._dayStart != null && this._dayEnd != null)
        {
            StringTokenizer st = new StringTokenizer(timeDayStart, ":");
            int minutesDayStart = timeToMins(st.nextToken(), st.nextToken());

            st = new StringTokenizer(timeDayEnd, ":");
            int minutesDayEnd = timeToMins(st.nextToken(), st.nextToken());

            int minutesWorkDuration = timeToMins(workingHours, workingMinutes);

            if (this.getLunchStart() != null && this.getLunchEnd() != null)
            {
                st = new StringTokenizer(timeLunchStart, ":");
                int minutesLunchStart = timeToMins(st.nextToken(), st.nextToken());
                st = new StringTokenizer(timeLunchEnd, ":");
                int minutesLunchEnd = timeToMins(st.nextToken(), st.nextToken());

                // TODO move to settings
                int minimumLunchDuration = 60;

                int minutesLunchDuration = Math.max(minutesLunchEnd - minutesLunchStart, minimumLunchDuration);

                minutesWorkDuration = minutesWorkDuration + minutesLunchDuration;
            }

            int minutesExtra = minutesDayEnd - (minutesDayStart + minutesWorkDuration);

            return minutesExtra;
        }
        else
        {
            return 0;
        }
    }

    private int timeToMins(String hour, String minute)
    {
        return Integer.parseInt(hour) * 60 + Integer.parseInt(minute);
    }

}
